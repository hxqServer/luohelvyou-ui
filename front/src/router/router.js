import VueRouter from 'vue-router'

//引入组件
import Index from '../pages'
import Home from '../pages/home/home'
import Login from '../pages/login/login'
import Register from '../pages/register/register'
import Center from '../pages/center/center'
import Messages from '../pages/messages/list'
import Storeup from '../pages/storeup/list'
import News from '../pages/news/news-list'
import NewsDetail from '../pages/news/news-detail'
import userList from '../pages/user/list'
import userDetail from '../pages/user/detail'
import userAdd from '../pages/user/add'
import culturalactivityList from '../pages/culturalactivity/list'
import culturalactivityDetail from '../pages/culturalactivity/detail'
import culturalactivityAdd from '../pages/culturalactivity/add'
import attractionclassificationList from '../pages/attractionclassification/list'
import attractionclassificationDetail from '../pages/attractionclassification/detail'
import attractionclassificationAdd from '../pages/attractionclassification/add'
import productclassificationList from '../pages/productclassification/list'
import productclassificationDetail from '../pages/productclassification/detail'
import productclassificationAdd from '../pages/productclassification/add'
import productinformationList from '../pages/productinformation/list'
import productinformationDetail from '../pages/productinformation/detail'
import productinformationAdd from '../pages/productinformation/add'
import attractioninformationList from '../pages/attractioninformation/list'
import attractioninformationDetail from '../pages/attractioninformation/detail'
import attractioninformationAdd from '../pages/attractioninformation/add'
import hotelinformationList from '../pages/hotelinformation/list'
import hotelinformationDetail from '../pages/hotelinformation/detail'
import hotelinformationAdd from '../pages/hotelinformation/add'
import tooltypeList from '../pages/tooltype/list'
import tooltypeDetail from '../pages/tooltype/detail'
import tooltypeAdd from '../pages/tooltype/add'
import traveltoolsList from '../pages/traveltools/list'
import traveltoolsDetail from '../pages/traveltools/detail'
import traveltoolsAdd from '../pages/traveltools/add'
import linetypeList from '../pages/linetype/list'
import linetypeDetail from '../pages/linetype/detail'
import linetypeAdd from '../pages/linetype/add'
import routeplanningList from '../pages/routeplanning/list'
import routeplanningDetail from '../pages/routeplanning/detail'
import routeplanningAdd from '../pages/routeplanning/add'
import hotelreservationList from '../pages/hotelreservation/list'
import hotelreservationDetail from '../pages/hotelreservation/detail'
import hotelreservationAdd from '../pages/hotelreservation/add'
import ticketorderList from '../pages/ticketorder/list'
import ticketorderDetail from '../pages/ticketorder/detail'
import ticketorderAdd from '../pages/ticketorder/add'
import productorderList from '../pages/productorder/list'
import productorderDetail from '../pages/productorder/detail'
import productorderAdd from '../pages/productorder/add'
import toolorderList from '../pages/toolorder/list'
import toolorderDetail from '../pages/toolorder/detail'
import toolorderAdd from '../pages/toolorder/add'
import eventregistrationList from '../pages/eventregistration/list'
import eventregistrationDetail from '../pages/eventregistration/detail'
import eventregistrationAdd from '../pages/eventregistration/add'
import tourismstrategyList from '../pages/tourismstrategy/list'
import tourismstrategyDetail from '../pages/tourismstrategy/detail'
import tourismstrategyAdd from '../pages/tourismstrategy/add'
import Forum from '../pages/forum/list'
import ForumAdd from '../pages/forum/add'
import ForumDetail from '../pages/forum/detail'
import MyForumList from '../pages/forum/myForumList'
import xianluyudingList from '../pages/xianluyuding/list'
import xianluyudingDetail from '../pages/xianluyuding/detail'
import xianluyudingAdd from '../pages/xianluyuding/add'
import routeplanningtuijianList from '../pages/routeplanningtuijian/list'
import routeplanningtuijianDetail from '../pages/routeplanningtuijian/detail'
import routeplanningtuijianAdd from '../pages/routeplanningtuijian/add'






const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
	return originalPush.call(this, location).catch(err => err)
}

//配置路由
export default new VueRouter({
	routes:[
		{
      path: '/',
      redirect: '/index/home'
    },
		{
			path: '/index',
			component: Index,
			children:[
				{
					path: 'home',
					component: Home
				},
				{
					path: 'center',
					component: Center,
				},
				{
					path: 'messages',
					component: Messages
				},
				{
					path: 'storeup',
					component: Storeup
				},
				{
					path: 'news',
					component: News
				},
				{
					path: 'newsDetail',
					component: NewsDetail
				},
				{
					path: 'user',
					component: userList
				},
				{
					path: 'userDetail',
					component: userDetail
				},
				{
					path: 'userAdd',
					component: userAdd
				},
				{
					path: 'forum',
					component: Forum
				},
				{
					path: 'forumAdd',
					component: ForumAdd
				},
				{
					path: 'forumDetail',
					component: ForumDetail
				},
				{
					path: 'xianluyuding',
					component: xianluyudingList
				},
				{
					path: 'xianluyudingDetail',
					component: xianluyudingDetail
				},
				{
					path: 'xianluyudingAdd',
					component: xianluyudingAdd
				},


				{
					path: 'myForumList',
					component: MyForumList
				},
				{
					path: 'culturalactivity',
					component: culturalactivityList
				},
				{
					path: 'culturalactivityDetail',
					component: culturalactivityDetail
				},
				{
					path: 'culturalactivityAdd',
					component: culturalactivityAdd
				},
				{
					path: 'attractionclassification',
					component: attractionclassificationList
				},
				{
					path: 'attractionclassificationDetail',
					component: attractionclassificationDetail
				},
				{
					path: 'attractionclassificationAdd',
					component: attractionclassificationAdd
				},
				{
					path: 'productclassification',
					component: productclassificationList
				},
				{
					path: 'productclassificationDetail',
					component: productclassificationDetail
				},
				{
					path: 'productclassificationAdd',
					component: productclassificationAdd
				},
				{
					path: 'productinformation',
					component: productinformationList
				},
				{
					path: 'productinformationDetail',
					component: productinformationDetail
				},
				{
					path: 'productinformationAdd',
					component: productinformationAdd
				},
				{
					path: 'attractioninformation',
					component: attractioninformationList
				},
				{
					path: 'attractioninformationDetail',
					component: attractioninformationDetail
				},
				{
					path: 'attractioninformationAdd',
					component: attractioninformationAdd
				},
				{
					path: 'hotelinformation',
					component: hotelinformationList
				},
				{
					path: 'hotelinformationDetail',
					component: hotelinformationDetail
				},
				{
					path: 'hotelinformationAdd',
					component: hotelinformationAdd
				},
				{
					path: 'tooltype',
					component: tooltypeList
				},
				{
					path: 'tooltypeDetail',
					component: tooltypeDetail
				},
				{
					path: 'tooltypeAdd',
					component: tooltypeAdd
				},
				{
					path: 'traveltools',
					component: traveltoolsList
				},
				{
					path: 'traveltoolsDetail',
					component: traveltoolsDetail
				},
				{
					path: 'traveltoolsAdd',
					component: traveltoolsAdd
				},
				{
					path: 'linetype',
					component: linetypeList
				},
				{
					path: 'linetypeDetail',
					component: linetypeDetail
				},
				{
					path: 'linetypeAdd',
					component: linetypeAdd
				},


				{
					path: 'routeplanningtuijian',
					component: routeplanningtuijianList
				},
				{
					path: 'routeplanningtuijianDetail',
					component: routeplanningtuijianDetail
				},
				{
					path: 'routeplanningtuijianAdd',
					component: routeplanningtuijianAdd
				},

				{
					path: 'routeplanning',
					component: routeplanningList
				},
				{
					path: 'routeplanningDetail',
					component: routeplanningDetail
				},
				{
					path: 'routeplanningAdd',
					component: routeplanningAdd
				},
				{
					path: 'hotelreservation',
					component: hotelreservationList
				},
				{
					path: 'hotelreservationDetail',
					component: hotelreservationDetail
				},
				{
					path: 'hotelreservationAdd',
					component: hotelreservationAdd
				},
				{
					path: 'ticketorder',
					component: ticketorderList
				},
				{
					path: 'ticketorderDetail',
					component: ticketorderDetail
				},
				{
					path: 'ticketorderAdd',
					component: ticketorderAdd
				},
				{
					path: 'productorder',
					component: productorderList
				},
				{
					path: 'productorderDetail',
					component: productorderDetail
				},
				{
					path: 'productorderAdd',
					component: productorderAdd
				},
				{
					path: 'toolorder',
					component: toolorderList
				},
				{
					path: 'toolorderDetail',
					component: toolorderDetail
				},
				{
					path: 'toolorderAdd',
					component: toolorderAdd
				},
				{
					path: 'eventregistration',
					component: eventregistrationList
				},
				{
					path: 'eventregistrationDetail',
					component: eventregistrationDetail
				},
				{
					path: 'eventregistrationAdd',
					component: eventregistrationAdd
				},
				{
					path: 'tourismstrategy',
					component: tourismstrategyList
				},
				{
					path: 'tourismstrategyDetail',
					component: tourismstrategyDetail
				},
				{
					path: 'tourismstrategyAdd',
					component: tourismstrategyAdd
				},
			]
		},
		{
			path: '/login',
			component: Login
		},
		{
			path: '/register',
			component: Register
		},
	]
})
