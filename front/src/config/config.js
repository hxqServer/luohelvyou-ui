export default {
    baseUrl: 'http://localhost:8080/luohelvyou/',
    indexNav: [
        {
            name: '首页',
            url: '/index/home'
        },


        {
            name: '景点信息',
            url: '/index/attractioninformation'
        },
        
        {
            name: '线路信息',
            url: '/index/routeplanning'
        },
        {
            name: '论坛交流',
            url: '/index/forum'
        },
        {
            name: '公告信息',
            url: '/index/news'
        },
        
    ]
}
