const base = {
    get() {
        return {
            url : "http://localhost:8080/luohelvyou/",
            name: "luohelvyou",
            // 退出到首页链接
            indexUrl: 'http://localhost:8080/luohelvyou/front/dist/index.html'
        };
    },
    getProjectName(){
        return {
            projectName: "基于Java的漯河市旅游业信息管理平台"
        } 
    }
}
export default base
