const menu = {
    list() {
        return [{
            "backMenu": [{
                "child": [{
                    "appFrontIcon": "cuIcon-taxi",
                    "buttons": ["新增", "查看", "修改", "删除"],
                    "menu": "用户",
                    "menuJump": "列表",
                    "tableName": "user"
                }], "menu": "用户管理"
            },   {
                "child": [{
                    "appFrontIcon": "cuIcon-send",
                    "buttons": ["新增", "查看", "修改", "删除"],
                    "menu": "景点分类",
                    "menuJump": "列表",
                    "tableName": "attractionclassification"
                }], "menu": "景点分类管理"
            },   {
                "child": [{
                    "appFrontIcon": "cuIcon-medal",
                    "buttons": ["新增", "查看", "修改", "删除", "景点门票数量统计", "查看评论", "首页总数", "首页统计"],
                    "menu": "景点信息",
                    "menuJump": "列表",
                    "tableName": "attractioninformation"
                }], "menu": "景点信息管理"
            },    {
                "child": [{
                    "appFrontIcon": "cuIcon-keyboard",
                    "buttons": ["新增", "查看", "修改", "删除"],
                    "menu": "线路类型",
                    "menuJump": "列表",
                    "tableName": "linetype"
                }], "menu": "线路类型管理"
            }, {
                "child": [{
                    "appFrontIcon": "cuIcon-attentionfavor",
                    "buttons": ["新增", "查看", "修改", "删除", "查看评论"],
                    "menu": "线路信息",
                    "menuJump": "列表",
                    "tableName": "routeplanning"
                }], "menu": "线路信息管理"
            },
                {
                    "child": [{
                        "appFrontIcon": "cuIcon-news",
                        "buttons": [ "查看", "修改", "删除", "审核" ],
                        "menu": "线路预订",
                        "menuJump": "列表",
                        "tableName": "xianluyuding"
                    }], "menu": "线路预订管理"
                },  {
                "child": [{
                    "appFrontIcon": "cuIcon-pay",
                    "buttons": ["审核", "删除", "查看"],
                    "menu": "门票订单",
                    "menuJump": "列表",
                    "tableName": "ticketorder"
                }], "menu": "门票订单管理"
            }, {
                "child": [{
                    "appFrontIcon": "cuIcon-group",
                    "buttons": ["查看", "修改", "删除", "查看评论"],
                    "menu": "交流论坛",
                    "tableName": "forum"
                }], "menu": "交流论坛"
            },  {
                "child": [{
                    "appFrontIcon": "cuIcon-shop",
                    "buttons": ["查看", "修改"],
                    "menu": "轮播图管理",
                    "tableName": "config"
                }, {
                    "appFrontIcon": "cuIcon-news",
                    "buttons": ["新增", "查看", "修改", "删除"],
                    "menu": "公告信息",
                    "tableName": "news"
                }, {"appFrontIcon": "cuIcon-list", "buttons": ["查看", "修改"], "menu": "关于我们", "tableName": "aboutus"}],
                "menu": "系统管理"
            }],
            "frontMenu": [{
                "child": [{
                    "appFrontIcon": "cuIcon-send",
                    "buttons": ["查看", "活动报名"],
                    "menu": "文化活动",
                    "menuJump": "列表",
                    "tableName": "culturalactivity"
                }], "menu": "文化活动模块"
            }, {
                "child": [{
                    "appFrontIcon": "cuIcon-qrcode",
                    "buttons": ["查看", "购买产品"],
                    "menu": "产品信息",
                    "menuJump": "列表",
                    "tableName": "productinformation"
                }], "menu": "产品信息模块"
            }, {
                "child": [{
                    "appFrontIcon": "cuIcon-newshot",
                    "buttons": ["查看", "购买门票"],
                    "menu": "景点信息",
                    "menuJump": "列表",
                    "tableName": "attractioninformation"
                }], "menu": "景点信息模块"
            }, {
                "child": [{
                    "appFrontIcon": "cuIcon-clothes",
                    "buttons": ["查看", "预定酒店"],
                    "menu": "酒店信息",
                    "menuJump": "列表",
                    "tableName": "hotelinformation"
                }], "menu": "酒店信息模块"
            }, {
                "child": [{
                    "appFrontIcon": "cuIcon-pic",
                    "buttons": ["查看", "购买工具"],
                    "menu": "出行工具",
                    "menuJump": "列表",
                    "tableName": "traveltools"
                }], "menu": "出行工具模块"
            }, {
                "child": [{
                    "appFrontIcon": "cuIcon-form",
                    "buttons": ["查看"],
                    "menu": "线路信息",
                    "menuJump": "列表",
                    "tableName": "routeplanning"
                }], "menu": "线路信息模块"
            }, {
                "child": [{
                    "appFrontIcon": "cuIcon-rank",
                    "buttons": ["查看", "查看评论"],
                    "menu": "旅游攻略",
                    "menuJump": "列表",
                    "tableName": "tourismstrategy"
                }], "menu": "旅游攻略模块"
            }],
            "hasBackLogin": "是",
            "hasBackRegister": "否",
            "hasFrontLogin": "否",
            "hasFrontRegister": "否",
            "roleName": "管理员",
            "tableName": "users"
        }, {
            "backMenu": [ {
                "child": [{
                    "appFrontIcon": "cuIcon-pay",
                    "buttons": ["支付", "删除", "查看"],
                    "menu": "门票订单",
                    "menuJump": "列表",
                    "tableName": "ticketorder"
                }], "menu": "门票订单管理"
            },
                {
                    "child": [{
                        "appFrontIcon": "cuIcon-news",
                        "buttons": [ "查看",   "删除" ],
                        "menu": "线路预订",
                        "menuJump": "列表",
                        "tableName": "xianluyuding"
                    }], "menu": "线路预订管理"
                }, ],
            "frontMenu": [{
                "child": [{
                    "appFrontIcon": "cuIcon-send",
                    "buttons": ["查看", "活动报名"],
                    "menu": "文化活动",
                    "menuJump": "列表",
                    "tableName": "culturalactivity"
                }], "menu": "文化活动模块"
            }, {
                "child": [{
                    "appFrontIcon": "cuIcon-qrcode",
                    "buttons": ["查看", "购买产品"],
                    "menu": "产品信息",
                    "menuJump": "列表",
                    "tableName": "productinformation"
                }], "menu": "产品信息模块"
            }, {
                "child": [{
                    "appFrontIcon": "cuIcon-newshot",
                    "buttons": ["查看", "购买门票"],
                    "menu": "景点信息",
                    "menuJump": "列表",
                    "tableName": "attractioninformation"
                }], "menu": "景点信息模块"
            }, {
                "child": [{
                    "appFrontIcon": "cuIcon-clothes",
                    "buttons": ["查看", "预定酒店"],
                    "menu": "酒店信息",
                    "menuJump": "列表",
                    "tableName": "hotelinformation"
                }], "menu": "酒店信息模块"
            }, {
                "child": [{
                    "appFrontIcon": "cuIcon-pic",
                    "buttons": ["查看", "购买工具"],
                    "menu": "出行工具",
                    "menuJump": "列表",
                    "tableName": "traveltools"
                }], "menu": "出行工具模块"
            }, {
                "child": [{
                    "appFrontIcon": "cuIcon-form",
                    "buttons": ["查看"],
                    "menu": "线路信息",
                    "menuJump": "列表",
                    "tableName": "routeplanning"
                }], "menu": "线路信息模块"
            }, {
                "child": [{
                    "appFrontIcon": "cuIcon-rank",
                    "buttons": ["查看", "查看评论"],
                    "menu": "旅游攻略",
                    "menuJump": "列表",
                    "tableName": "tourismstrategy"
                }], "menu": "旅游攻略模块"
            }],
            "hasBackLogin": "是",
            "hasBackRegister": "否",
            "hasFrontLogin": "是",
            "hasFrontRegister": "是",
            "roleName": "用户",
            "tableName": "user"
        }]
    }
}
export default menu;
