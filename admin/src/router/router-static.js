import Vue from 'vue';
//配置路由
import VueRouter from 'vue-router'
Vue.use(VueRouter);
//1.创建组件
import Index from '@/views/index'
import Home from '@/views/home'
import Login from '@/views/login'
import NotFound from '@/views/404'
import UpdatePassword from '@/views/update-password'
import pay from '@/views/pay'
import register from '@/views/register'
import center from '@/views/center'
    import discusshotelinformation from '@/views/modules/discusshotelinformation/list'
    import traveltools from '@/views/modules/traveltools/list'
    import hotelreservation from '@/views/modules/hotelreservation/list'
    import linetype from '@/views/modules/linetype/list'
    import tourismstrategy from '@/views/modules/tourismstrategy/list'
    import discussproductinformation from '@/views/modules/discussproductinformation/list'
    import productclassification from '@/views/modules/productclassification/list'
    import discussrouteplanning from '@/views/modules/discussrouteplanning/list'
    import eventregistration from '@/views/modules/eventregistration/list'
    import discussculturalactivity from '@/views/modules/discussculturalactivity/list'
    import productinformation from '@/views/modules/productinformation/list'
    import attractionclassification from '@/views/modules/attractionclassification/list'
    import discusstourismstrategy from '@/views/modules/discusstourismstrategy/list'
    import news from '@/views/modules/news/list'
    import aboutus from '@/views/modules/aboutus/list'
    import toolorder from '@/views/modules/toolorder/list'
    import discussattractioninformation from '@/views/modules/discussattractioninformation/list'
    import hotelinformation from '@/views/modules/hotelinformation/list'
    import productorder from '@/views/modules/productorder/list'
    import ticketorder from '@/views/modules/ticketorder/list'
    import discusstraveltools from '@/views/modules/discusstraveltools/list'
    import culturalactivity from '@/views/modules/culturalactivity/list'
    import messages from '@/views/modules/messages/list'
    import tooltype from '@/views/modules/tooltype/list'
    import user from '@/views/modules/user/list'
    import config from '@/views/modules/config/list'
    import routeplanning from '@/views/modules/routeplanning/list'
    import attractioninformation from '@/views/modules/attractioninformation/list'
import forum from '@/views/modules/forum/list'
import xianluyuding from '@/views/modules/xianluyuding/list'


//2.配置路由   注意：名字
const routes = [{
    path: '/index',
    name: '系统首页',
    component: Index,
    children: [{
      // 这里不设置值，是把main作为默认页面
      path: '/',
      name: '系统首页',
      component: Home,
      meta: {icon:'', title:'center'}
    }, {
      path: '/updatePassword',
      name: '修改密码',
      component: UpdatePassword,
      meta: {icon:'', title:'updatePassword'}
    }, {
      path: '/pay',
      name: '支付',
      component: pay,
      meta: {icon:'', title:'pay'}
    }, {
      path: '/center',
      name: '个人信息',
      component: center,
      meta: {icon:'', title:'center'}
    }
      ,{
	path: '/discusshotelinformation',
        name: '酒店信息',
        component: discusshotelinformation
      }
      ,{
	path: '/traveltools',
        name: '出行工具',
        component: traveltools
      }
      ,{
	path: '/hotelreservation',
        name: '酒店预订',
        component: hotelreservation
      }
      ,{
	path: '/linetype',
        name: '线路类型',
        component: linetype
      }
      ,{
	path: '/tourismstrategy',
        name: '旅游攻略',
        component: tourismstrategy
      } ,{
            path: '/forum',
            name: '交流论坛',
            component: forum
        }
      ,{
	path: '/discussproductinformation',
        name: '产品信息',
        component: discussproductinformation
      }
      ,{
            path: '/xianluyuding',
            name: '线路预订',
            component: xianluyuding
        },{
	path: '/productclassification',
        name: '产品分类',
        component: productclassification
      }
      ,{
	path: '/discussrouteplanning',
        name: '线路信息',
        component: discussrouteplanning
      }
      ,{
	path: '/eventregistration',
        name: '活动报名',
        component: eventregistration
      }
      ,{
	path: '/discussculturalactivity',
        name: '文化活动',
        component: discussculturalactivity
      }
      ,{
	path: '/productinformation',
        name: '产品信息',
        component: productinformation
      }
      ,{
	path: '/attractionclassification',
        name: '景点分类',
        component: attractionclassification
      }
      ,{
	path: '/discusstourismstrategy',
        name: '旅游攻略',
        component: discusstourismstrategy
      }
      ,{
	path: '/news',
        name: '公告信息',
        component: news
      }
      ,{
	path: '/aboutus',
        name: '关于我们',
        component: aboutus
      }
      ,{
	path: '/toolorder',
        name: '工具订单',
        component: toolorder
      }
      ,{
	path: '/discussattractioninformation',
        name: '景点信息',
        component: discussattractioninformation
      }
      ,{
	path: '/hotelinformation',
        name: '酒店信息',
        component: hotelinformation
      }
      ,{
	path: '/productorder',
        name: '产品订单',
        component: productorder
      }
      ,{
	path: '/ticketorder',
        name: '门票订单',
        component: ticketorder
      }
      ,{
	path: '/discusstraveltools',
        name: '出行工具',
        component: discusstraveltools
      }
      ,{
	path: '/culturalactivity',
        name: '文化活动',
        component: culturalactivity
      }
      ,{
	path: '/messages',
        name: '留言板管理',
        component: messages
      }
      ,{
	path: '/tooltype',
        name: '工具类型',
        component: tooltype
      }
      ,{
	path: '/user',
        name: '用户',
        component: user
      }
      ,{
	path: '/config',
        name: '轮播图管理',
        component: config
      }
      ,{
	path: '/routeplanning',
        name: '线路信息',
        component: routeplanning
      }
      ,{
	path: '/attractioninformation',
        name: '景点信息',
        component: attractioninformation
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {icon:'', title:'login'}
  },
  {
    path: '/register',
    name: 'register',
    component: register,
    meta: {icon:'', title:'register'}
  },
  {
    path: '/',
    name: '系统首页',
    redirect: '/index'
  }, /*默认跳转路由*/
  {
    path: '*',
    component: NotFound
  }
]
//3.实例化VueRouter  注意：名字
const router = new VueRouter({
  mode: 'hash',
  /*hash模式改为history*/
  routes // （缩写）相当于 routes: routes
})
const originalPush = VueRouter.prototype.push
//修改原型对象中的push方法
VueRouter.prototype.push = function push(location) {
   return originalPush.call(this, location).catch(err => err)
}
export default router;
